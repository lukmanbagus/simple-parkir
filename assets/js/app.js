(function($) {
  var App = {
    init: function() {
      App.SubmitAjax();
    },
    SubmitAjax: function() {
      // Submit Kendaraan Masuk
      $('button[name=btn-masuk]').click(function() {
        var nopol = $('input[name=no_pol]').val();
        var jenis = $('select[name=jenis]').val();
        var merek = $('input[name=merek]').val();
        var url = base_url() + '/transaksi/store';

        $.ajax({
          method: 'post',
          url: url,
          data: {jenis: jenis, nopol: nopol, merek: merek},
          success: function(data) {
            $('#datatable').reload();
          }
        });
      });

      function base_url() {
        var hostname = window.location.hostname;
        var port = window.location.port;
        var base_url = 'http://'+hostname+':'+port+'/parkir';
        return base_url;
      }
    }
  }

  $(function(){
    App.init();
  });
})(jQuery)
