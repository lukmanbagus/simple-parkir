<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('activated') != null) {
			// saat user sudah login, akan dialihkan ke menu dashboard ketika
			// user mencoba ke menu login kembali
			redirect(base_url() . 'dashboard');
		}
	}


	public function index()
	{
		$this->load->view('welcome');
	}
}
