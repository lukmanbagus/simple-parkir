<?php
  defined('BASEPATH') OR exit('No direct script access allowed');

  /**
   * Tarif Controller
   */
  class Petugas extends MY_Controller
  {

    public function __construct()
    {
      parent::__construct();
      $this->load->model('PetugasModel');
    }

    public function index()
    {
      $data['para_petugas'] = $this->PetugasModel->get();

      $this->template('index',$data);
    }

    public function template($page,$data = null)
    {
      $this->load->view('templates/header');
      $this->load->view('petugas/'.$page,$data);
      $this->load->view('templates/footer');
    }

    public function create()
    {
      $this->template('create');
    }

    public function store()
    {
      $data = [
        'nama' => $this->input->post('nama'),
        'alamat' => $this->input->post('alamat'),
        'jenis_kelamin' => $this->input->post('jenis_kelamin'),
        'username' => $this->input->post('username'),
        'password' => md5($this->input->post('password')),
        'level' => $this->input->post('level')
      ];

      $this->PetugasModel->insert($data);

      redirect(base_url() . 'petugas');
    }

    public function edit($id)
    {
      $data['petugas'] = $this->PetugasModel->getById($id);
      $this->template('edit',$data);
    }

    public function update($id)
    {
      $data = [
        'nama' => $this->input->post('nama'),
        'alamat' => $this->input->post('alamat'),
        'jenis_kelamin' => $this->input->post('jenis_kelamin'),
        'username' => $this->input->post('username'),
        'level' => $this->input->post('level')
      ];

      if ($this->input->post('password') != '') {
        $data['password'] = md5($this->input->post('password'));
      }

      $this->PetugasModel->update($id,$data);

      redirect(base_url() . 'petugas');
    }

    public function hapus($id)
    {
      $this->PetugasModel->delete($id);

      redirect(base_url() . 'petugas');
    }
  }
