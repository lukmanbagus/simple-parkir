<?php
  defined('BASEPATH') OR exit('No direct script access allowed');

  /**
   * Tarif Controller
   */
  class Login extends CI_Controller
  {

    public function __construct()
    {
      parent::__construct();
      $this->load->model('LoginModel');
    }

    public function index()
    {
      if ($this->input->post() == null) {
        redirect(base_url());
      }
      else {
        $this->LoginModel->attempt();
        redirect(base_url() . 'dashboard');
      }
    }

    public function logout()
    {
      $session = [
        'username',
        'nama',
        'level',
        'id_petugas',
        'activated'
      ];

      $this->session->unset_userdata($session);
      redirect(base_url());
    }

  }
