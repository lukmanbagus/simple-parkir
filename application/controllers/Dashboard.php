<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function index()
	{
		$this->template('index');
	}

	private function template($page,$data=null)
	{
		$this->load->view('templates/header');
		$this->load->view('dashboard/'.$page,$data);
		$this->load->view('templates/footer');
	}
}
