<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('TransaksiModel');
		$this->load->model('JenisKendaraanModel');
	}

	public function index()
	{
		$data['parkir_masuk'] = $this->TransaksiModel->getTransactionToday();
		$data['semua_parkir_masuk'] = $this->TransaksiModel->getAll();
		$this->template('index',$data);
	}

	public function keluar()
	{
		$data['parkir_keluar'] = $this->TransaksiModel->getOutTransactionToday();
		$data['semua_parkir_keluar'] = $this->TransaksiModel->getAllOut();
		$this->template('keluar',$data);
	}

	private function template($page,$data=null)
	{
		$this->load->view('templates/header');
		$this->load->view('transaksi/'.$page,$data);
		$this->load->view('templates/footer');
	}

  public function store()
  {
		$data = array(
			'no_pol' => $this->input->post('no_pol'),
			'id_jenis' => $this->input->post('jenis'),
			'merek' => $this->input->post('merek'),
			'tgl_masuk' => date('d-m-Y'),
			'jam_masuk' => date('h:m'),
			'status' => 'in'
		);

		$this->TransaksiModel->insert($data);

		redirect(base_url() . 'transaksi');
  }

	public function checkout($id)
	{
		$this->TransaksiModel->checkout($id);
		redirect(base_url() . 'transaksi');
	}

	public function cetak($id)
	{
		$data['kendaraan'] = $this->TransaksiModel->getById($id);
		$this->template('cetak',$data);
	}
}
