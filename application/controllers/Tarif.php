<?php
  defined('BASEPATH') OR exit('No direct script access allowed');

  /**
   * Tarif Controller
   */
  class Tarif extends MY_Controller
  {

    public function __construct()
    {
      parent::__construct();
      $this->load->model('TarifModel');
    }

    public function template($page,$data=null)
    {
      $this->load->view('templates/header');
      $this->load->view('tarif/'.$page,$data);
      $this->load->view('templates/footer');
    }

    public function index()
    {
      $data['tarifkendaraan'] = $this->TarifModel->get();

      $this->template('index',$data);
    }

    public function edit($id)
    {
      $data['tarif'] = $this->TarifModel->edit($id);
      $this->template('edit',$data);
    }

    public function update($id)
    {
      $data['tarif'] = $this->input->post('tarif');

      $this->TarifModel->update($id,$data);

      redirect(base_url() . 'tarif');
    }

  }
