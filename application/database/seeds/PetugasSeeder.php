<?php
          class PetugasSeeder extends Seeder {

              private $table = 'petugas';

              public function run() {
                  $this->db->truncate($this->table);

                  //seed records manually
                  $data = [
                      'id_petugas' => 1,
                      'username' => 'admin',
                      'nama' => 'Admin',
                      'alamat' => 'Tegal',
                      'jenis_kelamin' => 'Pria',
                      'level' => 'admin',
                      'password' => md5('admin')
                  ];
                  $this->db->insert($this->table, $data);

                  echo PHP_EOL;
              }
          }
