<?php
          class GolongantarifSeeder extends Seeder {

              private $table = 'golongantarif';

              public function run() {
                  $this->db->truncate($this->table);

                  // Tarif Mobil
                  $data = [
                      'id_tarif' => 1,
                      'tarif' => 4000
                  ];
                  $this->db->insert($this->table, $data);

                  // Tarif Motor
                  $data = [
                      'id_tarif' => 2,
                      'tarif' => 2000
                  ];
                  $this->db->insert($this->table, $data);

                  echo PHP_EOL;
              }
          }
