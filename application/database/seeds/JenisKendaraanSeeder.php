<?php
          class JenisKendaraanSeeder extends Seeder {

              private $table = 'jeniskendaraan';

              public function run() {
                  $this->db->truncate($this->table);

                  //seed records manually
                  $data = [
                      'id_jenis' => 1,
                      'jenis' => 'Mobil',
                      'id_tarif' => 1
                  ];
                  $this->db->insert($this->table, $data);

                  $data = [
                      'id_jenis' => 2,
                      'jenis' => 'Motor',
                      'id_tarif' => 2
                  ];
                  $this->db->insert($this->table, $data);

                  echo PHP_EOL;
              }
          }
