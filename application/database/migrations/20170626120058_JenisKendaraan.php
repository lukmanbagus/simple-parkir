
    <?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    class Migration_JenisKendaraan extends CI_Migration {
        public function up() {
            $this->dbforge->add_field(array(
                'id_jenis' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'auto_increment' => TRUE
                ),
                'jenis' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 50
                ),
                'id_tarif' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => TRUE
                )
            ));
            $this->dbforge->add_key('id_jenis',TRUE);
            $this->dbforge->create_table('jeniskendaraan');
        }

        public function down() {
            $this->dbforge->drop_table('jeniskendaraan');
        }
    }
