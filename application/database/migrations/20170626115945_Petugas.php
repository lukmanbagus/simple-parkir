
    <?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    class Migration_Petugas extends CI_Migration {
        public function up() {
            $this->dbforge->add_field(array(
                'id_petugas' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'auto_increment' => TRUE
                ),
                'username' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 50
                ),
                'nama' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 50
                ),
                'alamat' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 255
                ),
                'jenis_kelamin' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 10
                ),
                'level' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 10
                ),
                'password' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 255
                )
            ));
            $this->dbforge->add_key('id_petugas',TRUE);
            $this->dbforge->create_table('petugas');
        }

        public function down() {
            $this->dbforge->drop_table('petugas');
        }
    }
