<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Transaksi extends CI_Migration {
    public function up() {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'no_pol' => array(
                'type' => 'VARCHAR',
                'constraint' => 10
            ),
            'id_jenis' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'tgl_masuk' => array(
                'type' => 'VARCHAR',
                'constraint' => 10
            ),
            'jam_masuk' => array(
                'type' => 'VARCHAR',
                'constraint' => 10
            ),
            'tgl_keluar' => array(
                'type' => 'VARCHAR',
                'constraint' => 10,
                'null'  => TRUE
            ),
            'jam_keluar' => array(
                'type' => 'VARCHAR',
                'constraint' => 10
            ),
            'status' => array(
                'type' => 'VARCHAR',
                'constraint' => 20
            ),
            'merek' => array(
                'type' => 'VARCHAR',
                'constraint' => 50
            )
        ));
        $this->dbforge->add_key('id',TRUE);
        $this->dbforge->create_table('transaksi');
    }

    public function down() {
        $this->dbforge->drop_table('transaksi');
    }
}
