
    <?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    class Migration_Kendaraan extends CI_Migration {
        public function up() {
            $this->dbforge->add_field(array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'auto_increment' => TRUE
                ),
                'no_pol' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 10
                ),
                'id_jenis' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => TRUE
                )
            ));
            $this->dbforge->add_key('id',TRUE);
            $this->dbforge->create_table('kendaraan');
        }

        public function down() {
            $this->dbforge->drop_table('kendaraan');
        }
    }
