
    <?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    class Migration_GolonganTarif extends CI_Migration {
        public function up() {
            $this->dbforge->add_field(array(
                'id_tarif' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'auto_increment' => TRUE
                ),
                'tarif' => array(
                    'type' => 'DECIMAL',
                    'constraint' => 11
                )
            ));
            $this->dbforge->add_key('id_tarif',TRUE);
            $this->dbforge->create_table('golongantarif');
        }

        public function down() {
            $this->dbforge->drop_table('golongantarif');
        }
    }
