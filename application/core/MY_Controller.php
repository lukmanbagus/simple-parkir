<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		// memanggil function Auth setiap kali controller ini di muat
		$this->Auth();
	}

	public function Auth()
	{
		if ($this->session->userdata('activated') == null) {

			// pesan error ketika user belum login
			$this->session->set_flashdata('error','Anda harus login terlebih dahulu!');

			// redirect ke halaman utama
			redirect(base_url());
		}
		else {
			// load model LoginModel.php
			$this->load->model('LoginModel');

			// melakukan percobaan login
			$this->LoginModel->attempt();
		}
	}
}
