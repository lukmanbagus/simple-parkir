<div class="row">
  <div class="col-sm-4">
    <form action="<?= base_url() . 'laporan/pendapatan' ?>" method="get">
      <div class="panel panel-primary is-shadow">
        <div class="panel-heading">
          <h3 class="panel-title"><span class="glyphicon glyphicon-file"></span> &nbsp; Laporan</h3>
        </div>
        <div class="panel-body">
          <div class="form-group">
            <div class="col-sm-6">
              <select class="form-control" name="bulan">
                <option>Bulan</option>
                <option value="01">Januari</option>
                <option value="02">Februari</option>
                <option value="03">Maret</option>
                <option value="04">April</option>
                <option value="05">Mei</option>
                <option value="06">Juni</option>
                <option value="07">Juli</option>
                <option value="08">Agustus</option>
                <option value="09">September</option>
                <option value="10">Oktober</option>
                <option value="11">November</option>
                <option value="12">Desember</option>
              </select>
            </div>
            <div class="col-sm-6">
              <input type="text" name="tahun" class="form-control" placeholder="Tahun">
            </div>
          </div>
        </div>
        <div class="panel-footer">
          <button type="submit" class="btn btn-primary btn-sm">Cetak Laporan</button>
        </div>
      </div>
    </form>
  </div>
  <div class="col-sm-8">
    <div class="panel panel-primary is-shadow">
      <div class="panel-heading">
        <h3 class="panel-title"><span class="glyphicon glyphicon-file"></span> &nbsp; Data Transaksi</h3>
      </div>
      <div class="panel-body printarea">
        <?php
          $month = @$_GET['bulan'];
          $year = @$_GET['tahun'];
        ?>
        <?php if (isset($month) && isset($year)): ?>
          <h3><center>Laporan Pendapatan Parkir</center></h3>
          <table class="table table-condensed table-hover table-striped table-bordered">
            <thead>
              <tr>
                <th>No Parkir</th>
                <th>No Polisi</th>
                <th>Jenis</th>
                <th>Merek</th>
                <th>Jam Masuk</th>
                <th>Tarif</th>
              </tr>
            </thead>
            <tbody id="datatable">
              <?php foreach ($this->TransaksiModel->getTransactionByMonth($month,$year) as $parkir): ?>
                <tr>
                  <td><?= $parkir->id ?></td>
                  <td><?= $parkir->no_pol ?></td>
                  <td><?= $parkir->jenis ?></td>
                  <td><?= $parkir->merek ?></td>
                  <td><?= $parkir->jam_masuk ?></td>
                  <td>Rp <?= $parkir->tarif ?></td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
          <strong>Total Pendapatan Bulan Ini : <h3 style="color:red">Rp.<?= $this->TransaksiModel->getIncomeByMonth($month,$year)->pendapatan ?></h3></strong>
          <?php else: ?>
          <h3><center>Anda belum memilih bulan dan tahun</center></h3>
        <?php endif; ?>
      </div>
      <div class="panel-footer">
        <button type="button" class="btn btn-success" name="button" id="cetak">Cetak</button>
      </div>
    </div>
  </div>
</div>
