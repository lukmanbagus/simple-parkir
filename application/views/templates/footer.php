</div> <!-- End of container -->
  <script src="<?= base_url() . 'assets/js/jquery.min.js' ?>"></script>
  <script src="<?= base_url() . 'assets/js/bootstrap.min.js' ?>"></script>
  <script src="<?= base_url() . 'assets/js/jquery.PrintArea.js' ?>"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      var cetak = "<?= $this->uri->segment(2)?>";
      if ( cetak == 'cetak') {
        window.print();
        // Close the window after print
        setTimeout(function(){
          window.close();
        },1);
      }

      $('#cetak').bind('click',function(event){
        $('.printarea').printArea();
      })
    });
  </script>
</body>
</html>
