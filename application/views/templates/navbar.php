<?php
  $uri = $this->uri->segment(1)
?>
<nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?= base_url() ?>">Sistem Parkir</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="navbar">
      <ul class="nav navbar-nav">
        <li <?= ($uri == 'dashboard') ? 'class="active"' : '' ?>><a href="<?= base_url() . 'dashboard' ?>">Home</a></li>
        <?php if ($this->session->userdata('level') == 'admin'): ?>
          <li <?= ($uri == 'petugas') ? 'class="active"' : '' ?>><a href="<?= base_url() . 'petugas' ?>">Petugas</a></li>
          <li <?= ($uri == 'tarif') ? 'class="active"' : '' ?>><a href="<?= base_url() . 'tarif' ?>">Tarif Kendaraan</a></li>
        <?php endif; ?>
        <li class="dropdown <?= ($uri == 'transaksi') ? 'active' : '' ?>">
          <a href="<?= base_url() . 'transaksi' ?>" class="dropdown-toggle" data-toggle="dropdown" role="button">Transaksi <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?= base_url() . 'transaksi' ?>"><span class="glyphicon glyphicon-arrow-down"></span> Parkir Masuk</a></li>
            <li><a href="<?= base_url() . 'transaksi/keluar' ?>"><span class="glyphicon glyphicon-arrow-up"></span> Parkir Keluar</a></li>
          </ul>
        </li>
        <li class="dropdown <?= ($uri == 'laporan') ? 'active' : '' ?>">
          <a href="<?= base_url() . 'laporan' ?>" class="dropdown-toggle" data-toggle="dropdown" role="button">Laporan <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?= base_url() . 'laporan/kendaraan' ?>"> Kendaraan</a></li>
            <li><a href="<?= base_url() . 'laporan/pendapatan' ?>"> Pendapatan</a></li>
          </ul>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="<?= base_url() . 'logout' ?>">Keluar</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
