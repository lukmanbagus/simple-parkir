<div class="row">
  <div class="col-md-offset-2 col-md-8">
    <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title"><span class="glyphicon glyphicon-arrow-up"></span> Kendaraan Keluar</h3>
      </div>
      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#today" aria-controls="today" role="tab" data-toggle="tab">Hari ini</a></li>
        <li role="presentation"><a href="#all" aria-controls="all" role="tab" data-toggle="tab">Semuanya</a></li>
        <li><a href="<?= base_url() . 'transaksi' ?>">Kendaraan Masuk</a></li>
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="today">
          <table class="table table-condensed table-hover table-striped table-bordered">
            <thead>
              <tr>
                <th>No Parkir</th>
                <th>No Polisi</th>
                <th>Jenis</th>
                <th>Merek</th>
                <th>Jam Keluar</th>
                <th>Tarif</th>
              </tr>
            </thead>
            <tbody id="datatable">
              <?php foreach ($parkir_keluar as $parkir): ?>
                <tr>
                  <td><?= $parkir->id ?></td>
                  <td><?= $parkir->no_pol ?></td>
                  <td><?= $parkir->jenis ?></td>
                  <td><?= $parkir->merek ?></td>
                  <td><?= $parkir->jam_keluar ?></td>
                  <td>Rp <?= $parkir->tarif ?></td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
        <div role="tabpanel" class="tab-pane" id="all">
          <table class="table table-condensed table-hover table-striped table-bordered">
            <thead>
              <tr>
                <th>No Parkir</th>
                <th>No Polisi</th>
                <th>Jenis</th>
                <th>Merek</th>
                <th>Tanggal Masuk</th>
                <th>Tanggal Keluar</th>
                <th>Tarif</th>
              </tr>
            </thead>
            <tbody id="datatable">
              <?php foreach ($semua_parkir_keluar as $kendaraan): ?>
                <tr>
                  <td><?= $kendaraan->id ?></td>
                  <td><?= $kendaraan->no_pol ?></td>
                  <td><?= $kendaraan->jenis ?></td>
                  <td><?= $kendaraan->merek ?></td>
                  <td><?= nice_date($kendaraan->tgl_masuk,'d-m-Y') . ' ' . $kendaraan->jam_masuk ?></td>
                  <td><?= nice_date($kendaraan->tgl_keluar,'d-m-Y') . ' ' . $kendaraan->jam_keluar ?></td>
                  <td>Rp <?= $kendaraan->tarif ?></td>
                </tr>
              <?php endforeach; ?>
            </tbody>
            <tfoot>
              <tr>
                <td colspan="8"><strong>Total Kendaraan : <?= $this->TransaksiModel->countAllOut() ?></strong></td>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
      <div class="panel-footer">
        Jumlah Kendaraan Keluar Hari ini : <?= $this->TransaksiModel->countOutTransactionToday() ?>
      </div>
    </div>
  </div>
</div>
