<div class="print_area" style="border: 1px solid #ddd">
  <h4 class="page-header"><center>Tiket Parkir</center></h4>
  <table class="table table-bordered table-condensed">
    <tr>
      <th>No Polisi</th>
      <td><?= $kendaraan->no_pol ?></td>
    </tr>
    <tr>
      <th>Jenis</th>
      <td><?= $kendaraan->jenis ?></td>
    </tr>
    <tr>
      <th>Merek</th>
      <td><?= $kendaraan->merek ?></td>
    </tr>
    <tr>
      <th>Tanggal Masuk</th>
      <td><?= nice_date($kendaraan->tgl_masuk,'d-m-Y') . '  ' . $kendaraan->jam_masuk ?></td>
    </tr>
    <tr>
      <th>Tarif</th>
      <td>Rp <?= $kendaraan->tarif ?></td>
    </tr>
  </table>
</div>
