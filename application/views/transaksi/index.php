<div class="row">
  <div class="col-sm-4">
    <form action="<?= base_url() . 'transaksi/store' ?>" method="post">
      <div class="panel panel-primary is-shadow">
        <div class="panel-heading">
          <h3 class="panel-title"><span class="glyphicon glyphicon-pencil"></span> &nbsp; Parkir Masuk</h3>
        </div>
        <div class="panel-body">
          <div class="form-group">
            <label for="id_jenis">Jenis Kendaraan</label>
            <select class="form-control" name="jenis">
              <?php foreach ($this->JenisKendaraanModel->get() as $jenis): ?>
                <option value="<?= $jenis->id_jenis ?>"><?= $jenis->jenis ?></option>
              <?php endforeach; ?>
            </select>
            <!-- <p class="help-block">Help text here.</p> -->
          </div>
          <div class="form-group">
            <label for="no_pol">Plat Nomor</label>
            <input type="text" class="form-control" name="no_pol">
            <!-- <p class="help-block">Help text here.</p> -->
          </div>
          <div class="form-group">
            <label for="merek">Merek Kendaraan</label>
            <input type="text" class="form-control" name="merek">
            <!-- <p class="help-block">Help text here.</p> -->
          </div>
        </div>
        <div class="panel-footer">
          <button type="submit" name="btn-masuk" class="btn btn-primary btn-sm">Masuk</button>
        </div>
      </div>
    </form>
  </div>
  <div class="col-sm-8">
    <div class="panel panel-primary is-shadow">
      <div class="panel-heading">
        <h3 class="panel-title"><span class="glyphicon glyphicon-arrow-down"></span> &nbsp; Data Parkir Masuk</h3>
      </div>
      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#today" aria-controls="today" role="tab" data-toggle="tab">Hari ini</a></li>
        <li role="presentation"><a href="#all" aria-controls="all" role="tab" data-toggle="tab">Semuanya</a></li>
        <li><a href="<?= base_url() . 'transaksi/keluar' ?>">Kendaraan Keluar</a></li>
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="today">
          <table class="table table-condensed table-hover table-striped table-bordered">
            <thead>
              <tr>
                <th>No Parkir</th>
                <th>No Polisi</th>
                <th>Jenis</th>
                <th>Merek</th>
                <th>Jam Masuk</th>
                <th>Tarif</th>
                <th>Tools</th>
              </tr>
            </thead>
            <tbody id="datatable">
              <?php foreach ($parkir_masuk as $parkir): ?>
                <tr>
                  <td><?= $parkir->id ?></td>
                  <td><?= $parkir->no_pol ?></td>
                  <td><?= $parkir->jenis ?></td>
                  <td><?= $parkir->merek ?></td>
                  <td><?= $parkir->jam_masuk ?></td>
                  <td>Rp <?= $parkir->tarif ?></td>
                  <td>
                    <a href="<?= base_url() . 'transaksi/cetak/' . $parkir->id ?>" class="btn btn-xs btn-success" target="_blank">Cetak</a>
                    <a href="<?= base_url() . 'transaksi/parkir/keluar/' . $parkir->id ?>" class="btn btn-xs btn-warning">Keluar</a>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
        <div role="tabpanel" class="tab-pane" id="all">
          <table class="table table-condensed table-hover table-striped table-bordered">
            <thead>
              <tr>
                <th>No Parkir</th>
                <th>No Polisi</th>
                <th>Jenis</th>
                <th>Merek</th>
                <th>Tanggal Masuk</th>
                <th>Jam Masuk</th>
                <th>Tarif</th>
                <th>Tools</th>
              </tr>
            </thead>
            <tbody id="datatable">
              <?php foreach ($semua_parkir_masuk as $kendaraan): ?>
                <tr>
                  <td><?= $kendaraan->id ?></td>
                  <td><?= $kendaraan->no_pol ?></td>
                  <td><?= $kendaraan->jenis ?></td>
                  <td><?= $kendaraan->merek ?></td>
                  <td><?= nice_date($kendaraan->tgl_masuk,'d-m-Y') ?></td>
                  <td><?= $kendaraan->jam_masuk ?></td>
                  <td>Rp <?= $kendaraan->tarif ?></td>
                  <td>
                    <a href="#" class="btn btn-xs btn-warning">Keluar</a>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
            <tfoot>
              <tr>
                <td colspan="8"><strong>Total Kendaraan : <?= $this->TransaksiModel->countAll() ?></strong></td>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
      <div class="panel-footer">
        Jumlah kendaraan masuk hari ini : <?= $this->TransaksiModel->countTransactionToday() ?>
      </div>
    </div>
  </div>
</div>
