<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>RSI PKU Muhammadiyah</title>
	<link rel="stylesheet" href="<?= base_url() . 'assets/css/bootstrap.min.css' ?>">
	<style media="screen">
		body {
			padding-top: 70px;
		}
	</style>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-sm-offset-4 col-sm-4">
				<?php if ($this->session->flashdata('error') != null): ?>
					<div class="alert alert-danger">
						<?= $this->session->flashdata('error') ?>
					</div>
				<?php endif; ?>
				<h3 class="page-header">
					<center>
						<img src="<?= base_url('assets/images/logo.jpg') ?>" alt="" width="80"> <br><br>
						Sistem Informasi Parkir <br> <small>RS Islam PKU Muhammadiyah Kab Tegal</small>
					</center>
				</h3>
				<form class="form" action="<?= base_url() . 'login' ?>" method="post">
					<div class="form-group">
					  <label for="">Username</label>
					  <input type="text" name="username" class="form-control">
					  <!-- <p class="help-block">Help text here.</p> -->
					</div>
					<div class="form-group">
					  <label for="">Password</label>
					  <input type="password" name="password" class="form-control">
					  <!-- <p class="help-block">Help text here.</p> -->
					</div>
					<div class="form-group">
					  <button type="submit" class="btn btn-sm btn-primary">Login</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>
