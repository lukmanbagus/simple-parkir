<div class="row">
  <div class="col-md-offset-2 col-md-8">
    <div class="panel panel-primary is-shadow">
      <div class="panel-heading">
        <h3 class="panel-title"><span class="glyphicon glyphicon-"></span> &nbsp; Tarif Parkir Masuk</h3>
      </div>
      <table class="table table-condensed table-striped table-bordered table-hover">
        <thead>
          <tr>
            <th>No</th>
            <th>Jenis Kendaraan</th>
            <th>Tarif</th>
            <th>Tools</th>
          </tr>
        </thead>
        <tbody>
          <?php $no = 1;?>
          <?php foreach ($tarifkendaraan as $tarif): ?>
            <tr>
              <td><?= $no++ ?></td>
              <td><?= $tarif->jenis ?></td>
              <td>Rp. <?= $tarif->tarif ?></td>
              <td>
                <a href="<?= base_url() . 'tarif/edit/' . $tarif->id_tarif ?>" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-edit"></span></a>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
      <div class="panel-footer">

      </div>
    </div>
  </div>
</div>
