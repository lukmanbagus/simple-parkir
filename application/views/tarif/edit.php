<div class="row">
  <div class="col-md-offset-4 col-md-4">
    <form action="<?= base_url() . 'tarif/update/' . $tarif->id_tarif ?>" method="post">
      <div class="panel panel-primary is-shadow">
        <div class="panel-heading">
          <h3 class="panel-title"><span class="glyphicon glyphicon-money"></span> &nbsp; Tarif Parkir Masuk</h3>
        </div>
        <div class="panel-body">
          <div class="form-group">
            <label>Jenis</label>
            <input type="text" name="jenis" class="form-control" value="<?= $tarif->jenis ?>" readonly>
            <!-- <p class="help-block">Help text here.</p> -->
          </div>
          <div class="form-group">
            <label>Tarif</label>
            <input type="number" name="tarif" class="form-control" value="<?= $tarif->tarif ?>">
            <!-- <p class="help-block">Help text here.</p> -->
          </div>
        </div>
        <div class="panel-footer">
          <button type="submit" class="btn btn-success">Simpan</button>
          <a href="<?= base_url() . 'tarif' ?>" class="btn btn-default">Batal</a>
        </div>
      </div>
    </form>
  </div>
</div>
