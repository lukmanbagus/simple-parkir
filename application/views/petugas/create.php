<div class="row">
  <div class="col-md-offset-2 col-md-8">
    <form action="<?= base_url() . 'petugas/store'?>" method="post">
      <div class="panel panel-primary is-shadow">
        <div class="panel-heading">
          <h3 class="panel-title">Petugas Baru</h3>
        </div>
        <div class="panel-body">
          <div class="form-group">
            <label>Nama</label>
            <input type="text" class="form-control" name="nama">
            <!-- <p class="help-block">Help text here.</p> -->
          </div>
          <div class="form-group">
            <label>Alamat</label>
            <input type="text" class="form-control" name="alamat">
            <!-- <p class="help-block">Help text here.</p> -->
          </div>
          <div class="form-group">
            <label>Jenis Kelamin</label>
            <select class="form-control" name="jenis_kelamin">
              <option value="pria">Pria</option>
              <option value="wanita">Wanita</option>
            </select>
            <!-- <p class="help-block">Help text here.</p> -->
          </div>
          <div class="form-group">
            <label>Level</label>
            <select class="form-control" name="level">
              <option value="admin">Admin</option>
              <option value="petugas">Petugas</option>
            </select>
            <!-- <p class="help-block">Help text here.</p> -->
          </div>
          <hr>
          <div class="form-group">
            <label>Username</label>
            <input type="text" class="form-control" name="username">
            <!-- <p class="help-block">Help text here.</p> -->
          </div>
          <div class="form-group">
            <label>Password</label>
            <input type="text" class="form-control" name="password">
            <!-- <p class="help-block">Help text here.</p> -->
          </div>
        </div>
        <div class="panel-footer">
          <button type="submit" class="btn btn-success">Simpan</button>
          <a href="<?= base_url() . 'petugas' ?>" class="btn btn-default">Batal</a>
        </div>
      </div>
    </form>
  </div>
</div>
