<div class="row">
  <div class="col-md-offset-2 col-md-8">
    <div class="panel panel-primary is-shadow">
      <div class="panel-heading">
        <h3 class="panel-title">Petugas Pintu Parkir <a href="<?= base_url() . 'petugas/create' ?>" class="btn btn-xs btn-warning">Tambah Petugas</a></h3>
      </div>
      <table class="table table-condensed table-bordered table-striped table-responsive">
        <thead>
          <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Username</th>
            <th>Level</th>
            <th>Tools</th>
          </tr>
        </thead>
        <tbody>
          <?php $no = 1;?>
          <?php foreach ($para_petugas as $petugas): ?>
            <tr>
              <td><?= $no++ ?></td>
              <td><?= $petugas->nama ?></td>
              <td><?= $petugas->username ?></td>
              <td><?= $petugas->level ?></td>
              <td>
                <a href="<?= base_url() . 'petugas/edit/' . $petugas->id_petugas ?>" class="btn btn-xs btn-success"><span class="glyphicon glyphicon-edit"></span></a>
                <a href="<?= base_url() . 'petugas/hapus/' . $petugas->id_petugas ?>" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash"></span></a>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
      <div class="panel-footer">

      </div>
    </div>
  </div>
</div>
