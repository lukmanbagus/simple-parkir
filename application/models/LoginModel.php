<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Tarif Model
 */
class LoginModel extends CI_Model
{
  var $username;
  var $password;

  public function attempt()
  {
    if ($this->session->userdata('activated') == null) {
      $this->username = $this->input->post('username');
      $this->password = $this->input->post('password');
    }
    else {
      $this->username = $this->session->userdata('username');
      $this->password = $this->session->userdata('password');
    }

    $this->db->where('username',$this->username);
    if ($this->session->userdata('activated') == null) {
      $this->db->where('password',md5($this->password));
    }
    else {
      $this->db->where('password',$this->password);
    }
    $getRow = $this->db->get('petugas')->row();

    if ($getRow == null) {
      // Jika database akun terhapus
      $session = [
        'username',
        'nama',
        'level',
        'id_petugas',
        'activated'
      ];

      $this->session->unset_userdata($session);

      $this->session->set_flashdata('error','Username atau password salah!');
      redirect(base_url());
    }
    else {
      $data = [
        'username' => $getRow->username,
        'nama' => $getRow->nama,
        'password' => $getRow->password,
        'level' => $getRow->level,
        'id_petugas' => $getRow->id_petugas,
        'activated' => TRUE
      ];

      $this->session->set_userdata($data);
    }
  }

}
