<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Tarif Model
 */
class TransaksiModel extends CI_Model
{

  public function getTransactionToday()
  {
    $today = date('d-m-Y');

    $this->db->select('*');
    $this->db->from('transaksi');
    $this->db->join('jeniskendaraan',' jeniskendaraan.id_jenis = transaksi.id_jenis');
    $this->db->join('golongantarif',' golongantarif.id_tarif = jeniskendaraan.id_tarif');
    $this->db->where('tgl_masuk',$today);
    $this->db->where('status','in');
    $this->db->order_by('id','DESC');
    $query = $this->db->get();
    return $query->result();
  }

  public function getAll()
  {
    $this->db->select('*');
    $this->db->from('transaksi');
    $this->db->join('jeniskendaraan',' jeniskendaraan.id_jenis = transaksi.id_jenis');
    $this->db->join('golongantarif',' golongantarif.id_tarif = jeniskendaraan.id_tarif');
    $this->db->where('status','in');
    $this->db->order_by('tgl_masuk','DESC');
    $this->db->order_by('id','DESC');
    $query = $this->db->get();
    return $query->result();
  }

  public function getAllOut()
  {
    $this->db->select('*');
    $this->db->from('transaksi');
    $this->db->join('jeniskendaraan',' jeniskendaraan.id_jenis = transaksi.id_jenis');
    $this->db->join('golongantarif',' golongantarif.id_tarif = jeniskendaraan.id_tarif');
    $this->db->where('status','out');
    $this->db->order_by('tgl_keluar','DESC');
    $this->db->order_by('jam_keluar','DESC');
    $query = $this->db->get();
    return $query->result();
  }

  public function getOutTransactionToday()
  {
    $today = date('d-m-Y');

    $this->db->select('*');
    $this->db->from('transaksi');
    $this->db->join('jeniskendaraan',' jeniskendaraan.id_jenis = transaksi.id_jenis');
    $this->db->join('golongantarif',' golongantarif.id_tarif = jeniskendaraan.id_tarif');
    $this->db->where('tgl_keluar',$today);
    $this->db->where('status','out');
    $this->db->order_by('jam_keluar','DESC');
    $query = $this->db->get();
    return $query->result();
  }

  public function getTransactionByMonth($month,$year)
  {
    $this->db->select('*');
    $this->db->from('transaksi');
    $this->db->join('jeniskendaraan',' jeniskendaraan.id_jenis = transaksi.id_jenis');
    $this->db->join('golongantarif',' golongantarif.id_tarif = jeniskendaraan.id_tarif');
    $this->db->where('MONTH(STR_TO_DATE(tgl_masuk,"%d-%m-%Y"))',$month);
    $this->db->where('YEAR(STR_TO_DATE(tgl_masuk,"%d-%m-%Y"))',$year);
    $query = $this->db->get();
    return $query->result();
  }

  public function getVehicleByMonth($month,$year)
  {
    $this->db->select('*');
    $this->db->from('transaksi');
    $this->db->join('jeniskendaraan',' jeniskendaraan.id_jenis = transaksi.id_jenis');
    $this->db->join('golongantarif',' golongantarif.id_tarif = jeniskendaraan.id_tarif');
    $this->db->where('MONTH(STR_TO_DATE(tgl_masuk,"%d-%m-%Y"))',$month);
    $this->db->where('YEAR(STR_TO_DATE(tgl_masuk,"%d-%m-%Y"))',$year);
    $query = $this->db->get();
    return $query->result();
  }

  public function getIncomeByMonth($month,$year)
  {
    $this->db->select('SUM(tarif) as pendapatan');
    $this->db->from('transaksi');
    $this->db->join('jeniskendaraan',' jeniskendaraan.id_jenis = transaksi.id_jenis');
    $this->db->join('golongantarif',' golongantarif.id_tarif = jeniskendaraan.id_tarif');
    $this->db->where('MONTH(STR_TO_DATE(tgl_masuk,"%d-%m-%Y"))',$month);
    $this->db->where('YEAR(STR_TO_DATE(tgl_masuk,"%d-%m-%Y"))',$year);
    $query = $this->db->get();
    return $query->row();
  }

  public function countTransactionToday()
  {
    $today = date('d-m-Y');

    $this->db->where('tgl_masuk',$today);
    $this->db->from('transaksi');
    $this->db->where('status','in');
    echo $this->db->count_all_results();
  }

  public function countOutTransactionToday()
  {
    $today = date('d-m-Y');

    $this->db->where('tgl_keluar',$today);
    $this->db->from('transaksi');
    $this->db->where('status','out');
    echo $this->db->count_all_results();
  }

  public function countAll()
  {
    $this->db->where('status','in');
    echo $this->db->count_all_results('transaksi');
  }

  public function countAllOut()
  {
    $this->db->where('status','out');
    echo $this->db->count_all_results('transaksi');
  }

  public function insert($data)
  {
    $this->db->insert('transaksi',$data);
  }

  public function getById($id)
  {
    $this->db->where('id',$id);
    $this->db->join('jeniskendaraan',' jeniskendaraan.id_jenis = transaksi.id_jenis');
    $this->db->join('golongantarif',' golongantarif.id_tarif = jeniskendaraan.id_tarif');
    $data = $this->db->get('transaksi')->row();
    return $data;
  }

  public function checkout($id)
  {
    $data = [
      'status' => 'out',
      'tgl_keluar' => date('d-m-Y'),
      'jam_keluar' => date('h:m')
    ];

    $this->db->where('id',$id);
    $query = $this->db->update('transaksi',$data);
  }

}
