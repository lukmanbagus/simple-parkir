<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Tarif Model
 */
class JenisKendaraanModel extends CI_Model
{
  public function get()
  {
    $this->db->select('*');
    $this->db->from('jeniskendaraan');
    $query = $this->db->get();
    return $query->result();
  }
}
