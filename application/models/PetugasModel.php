<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Tarif Model
 */
class PetugasModel extends CI_Model
{

  public function get()
  {
    $data = $this->db->get('petugas')->result();
    return $data;
  }

  public function getById($id)
  {
    $this->db->where('id_petugas',$id);
    return $this->db->get('petugas')->row();
  }

  public function update($id,$data)
  {
    $this->db->where('id_petugas',$id);
    $this->db->update('petugas',$data);
  }

  public function insert($data)
  {
    $this->db->insert('petugas',$data);
  }

  public function delete($id)
  {
    $this->db->where('id_petugas',$id);
    $this->db->delete('petugas');
  }

}
