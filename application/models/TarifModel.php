<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Tarif Model
 */
class TarifModel extends CI_Model
{

  public function get()
  {
    $this->db->select('*');
    $this->db->from('golongantarif');
    $this->db->join('jeniskendaraan',' jeniskendaraan.id_tarif = golongantarif.id_tarif');
    $query = $this->db->get();
    return $query->result();
  }

  public function edit($id)
  {
    $this->db->select('*');
    $this->db->from('golongantarif');
    $this->db->where('golongantarif.id_tarif',$id);
    $this->db->join('jeniskendaraan','jeniskendaraan.id_tarif = golongantarif.id_tarif');
    return $this->db->get()->row();
    // return $result->row();
  }

  public function update($id,$data)
  {
    $this->db->where('id_tarif',$id);
    $this->db->update('golongantarif',$data);
  }

}
